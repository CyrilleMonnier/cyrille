'use strict';

var isformShown = true;
var mymap = null;
var marker = null;

function jsLoaded() {
    var jsloaded = document.querySelector('#is-js-loading');
    jsloaded.style.display = 'none';
    console.log('c\'est la balise que tu viens d\'enlever !', jsloaded);
    initFormEditNote();
}
jsLoaded();

function initFormEditNote() {
    var a = document.forms["form-edit-note"];
    a.addEventListener('submit', submitnote);
    a.addEventListener('reset', resetnote);

    document.forms["form-edit-note"].querySelectorAll('input:not(.btn),textarea,select').forEach(element => {
        element.addEventListener('focus', function(evt) {
            console.log(evt);
            evt.target.classList.remove('invalid-input');
        });
    });
    document.querySelector('#toggle-form-edit-note img').addEventListener('click', toggleeditform);


    isformShown = false;

    document.querySelector('.form-group').style.display =
        'none';

    mymap = L.map('mapid').setView([51.505, -0.09], 13);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="http://www.orsys.fr/">Orsys.fr</a>',
        id: 'mapbox.streets'
    }).addTo(mymap);
}

var Icon = L.icon({
    iconUrl: 'img/expand.png',
    iconSize: [40, 40], // size of the icon
});
marker = L.marker([51.5, -0.09], { icon: Icon }).addTo(mymap);
marker.bindPopup("<b>Hello !</b><br> I am a Popup !");

function toggleeditform() {
    isformShown = !isformShown;
    document.querySelector('.form-group').style.display = (isformShown) ? 'block' : 'none';
}

function submitnote(evt) {
    evt.preventDefault();
    console.log(evt);

    var objNewNote = checkValidityOfForm();
    if (objNewNote == null) return;
    var img = objNewNote.userimg;
    delete(objNewNote.userimg);
    var name = objNewNote.username;
    delete(objNewNote.username);

    notes_restcrud.create(objNewNote, '/notes', function(response) {
        var noteComplete = JSON.parse(response);
        noteComplete.userimg = img;
        noteComplete.userName = name;
        addNoteToList(noteComplete);
    });
}

function resetnote(evt) {
    evt.preventDefault();
    //document.forms['form-edit-note'].reset();
    var nodes = document.forms["form-edit-note"].querySelectorAll('input:not(.btn),textarea').forEach(function(element) { element.value = "" })

    document.forms["form-edit-note"].querySelectorAll('input:not(.btn),textarea,select').forEach(element => {
        element.classList.remove('invalid-input')
    });

    document.forms['form-edit-note']['input-user'].selectedIndex = -1;
}

function checkValidityOfForm() {
    var valid = true;
    var titreOfNote = document.forms["form-edit-note"]['input-titre'];
    if (titreOfNote.value.length <= 2) {
        valid = false;
        titreOfNote.classList.add('invalid-input');
    } else {
        titreOfNote.classList.remove('invalid-input');
        //titreOfNote = titreOfNote.value;
    }

    var date = document.forms["form-edit-note"]['input-date'];
    if (date.value == undefined || date.value == '') { //| Date.parse('2018-12-25') + time.value === undefined) {
        valid = false;
        date.classList.add('invalid-input');
    } else {
        date.classList.remove('invalid-input');
    }

    var time = document.forms["form-edit-note"]['input-time'];
    const regex = /^([01]\d|2[0-3]):[0-5]\d$/;
    if (regex.exec(time.value) == null) {
        valid = false;
        time.classList.add('invalid-input');
    } else {
        time.classList.remove('invalid-input');
    }

    var description = document.forms["form-edit-note"]['input-description'];
    if (description.value == "") {
        valid = false;
        description.classList.add('invalid-input');
    } else {
        description.classList.remove('invalid-input');
    }

    var user = document.forms["form-edit-note"]['input-user'];
    var userid = '';
    var userName = '';
    if (user.selectedIndex == -1 || user.value == '') {
        valid = false;
        user.classList.add('invalid-input');
    } else {
        user.classList.remove('invalid-input');
        var infosUser = user.options[user.selectedIndex].innerHTML.split(':');
        userid = infosUser[0];
        userName = infosUser[1];
    }

    console.log(titreOfNote, date, time, user, description);

    return (valid) ? {
        titre: titreOfNote.value,
        date: date.value,
        time: time.value,
        description: description.value,
        userimg: user.value,
        username: userName,
        user: userid
    } : null;
}

function addNoteToList(objNoteToAdd) {
    if (objNoteToAdd === undefined && ((objNoteToAdd = checkValidityOfForm()) == null)) return;
    console.log(objNoteToAdd);

    var maDivNote = document.createElement('div');
    maDivNote.classList.add('note');
    maDivNote.id = 'note-' + objNoteToAdd.id;
    maDivNote.innerHTML = '<div class="img-close"><img src="img/close.png" /></div><div class="note-user"><img src="img/usr/' + objNoteToAdd.userimg + '"><br/>' + objNoteToAdd.user + ':' + objNoteToAdd.username + '</div><div class="note-right"><div class="note-titre">' + objNoteToAdd.titre + '</div><div class="note-datetime">' + objNoteToAdd.date + ' à ' + objNoteToAdd.time + '</div></div><div class="note-description">' + objNoteToAdd.description + '</div>';
    maDivNote.querySelector('.img-close img').addEventListener('dblclick', deletenote);
    document.querySelector('#notes-container').appendChild(maDivNote);

    return true;
}

function deletenote(evt) {
    console.log(evt.target);
    var isOkToDelete = confirm('Souhaitez-vous vraiment supprimer cette note ?');
    if (isOkToDelete) {
        var idnote = evt.target.parentElement.parentElement.id.split('-')[1];
        notes_rescrud.delete('/notes/' + idnote, function() {
            alert('La suppression va être mise en place');
            evt.target.parentElement.parentElement.remove();
        });
    }
}






var notes_restcrud = new RestCRUD('http://localhost:7500');
//var notes_restcrud = new RestCRUD('http://192.168.43.100:7500')

notes_restcrud.read('/notes', function(responseText) {
    var myParseOfJson = JSON.parse(responseText);
    //   console.log(myParseOfJson);
    myParseOfJson.map(elem => {

        notes_restcrud.read('/users/' + elem.user, function(resp) {
            var userObjResponse = JSON.parse(resp);
            elem.userimg = userObjResponse.img;
            elem.username = userObjResponse.name;
            console.log(elem);
            addNoteToList(elem);
        });
    });
    //addNoteToList()
});